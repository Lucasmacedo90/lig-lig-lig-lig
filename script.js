let board = [
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0],

];
/* VARIAVEIS GLOBAIS */
var arrayTabela = [];
arrayTabela = [...board]

//divJogo recebe arrayTabela com appendChild()
const divJogo = document.getElementById('game');
const divMainContainer = document.getElementById('mainContainer')

//divFichaPreta e divFichaVermelha serao utilizadas em multiplas funçoes
var divFichaPreta;
var divFichaVermelha;

//jogadorAtual define quem é o jogador da vez
var jogadorAtual;

//colunas para criação das divColunas
var arrayColunas = new Array(7);

/* FUNCOES */

/*
LUCAS
function criaTabela()
    Popula o arrayTabela[7][6] com divElemento
    Atribui ID e Class a cada elemento criado
    Atribui style para divElemento ficar visivel
    Adiciona cada elemento a divJogo
*/



/*
LUCAS
function winCondition(arrayTabela)
    Recebe a tabela arrayTabela atualizada com as fichas para determinar se o jogador que fez o ultimo movimento ganhou ou nao
    Retorna TRUE caso ganhou, ou FALSE caso nao
    Caso TRUE , monta uma mensagem dizendo que o jogadorAtual ganhou e exibe um botao de RESET GAME
*/
function winCondition(arrayTabela) {
    // If we search past the edge we'll get a null pointer error
    const edgeX = arrayTabela[0].length - 3;
    const edgeY = arrayTabela.length - 3;
    let draw = true;

    //Draw Case
    for (let i = 0; i < arrayTabela.length; i++) {
        for (let j = 0; j < arrayTabela[0].length; j++) {
            if (arrayTabela[i][j] === 0) {
                draw = false;
                break;
            }
        }
    }

    if (draw === true) {
        console.log("Draw");
        const divVictory = document.createElement('div');
        divVictory.id = 'victory';
        divVictory.innerHTML = `empate !`;
        divMainContainer.appendChild(divVictory);
        return draw;
    }

    // HORIZONTAL
    // iterate each row
    for (let y = 0; y < arrayTabela.length; y++) {

        // iterate each cell in the row
        for (let x = 0; x < edgeX; x++) {
            let cell = arrayTabela[y][x];

            // Only check if cell is filled
            if (cell !== 0) {

                // Check the next two cells for the same value
                if (cell === arrayTabela[y][x + 1] && cell === arrayTabela[y][x + 2] && cell === arrayTabela[y][x + 3]) {
                    const divVictory = document.createElement('div')
                    divVictory.id = 'victory'
                    divVictory.innerHTML = `${jogadorAtual} você ganhou!`
                    divMainContainer.appendChild(divVictory)
                    let x = document.createElement('div');
                    x.className = 'meteor';
                    document.getElementById('alerta').appendChild(x);
                    return true
                }
            }
        }
    }

    // VERTICAL
    // iterate each row   
    for (let y = 0; y < edgeY; y++) {

        // iterate each cell in the row
        for (let x = 0; x < arrayTabela[0].length; x++) {
            cell = arrayTabela[y][x];

            // Only check if cell is filled
            if (cell !== 0) {

                // Check the next two cells for the same value
                if (cell === arrayTabela[y + 1][x] && cell === arrayTabela[y + 2][x] && cell === arrayTabela[y + 3][x]) {
                    const divVictory = document.createElement('div')
                    divVictory.id = 'victory'
                    divVictory.innerHTML = `${jogadorAtual} você ganhou!`
                    divMainContainer.appendChild(divVictory)
                    let x = document.createElement('div');
                     x.className = 'meteor';
                    document.getElementById('alerta').appendChild(x);
                    return true
                }
            }
        }
    }

    // DIAGONAL (DOWN RIGHT)
    // iterate each row   
    for (let y = 0; y < edgeY; y++) {

        // iterate each cell in the row
        for (let x = 0; x < edgeX; x++) {
            cell = arrayTabela[y][x];

            // Only check if cell is filled
            if (cell !== 0) {

                // Check the next two cells for the same value
                if (cell === arrayTabela[y + 1][x + 1] && cell === arrayTabela[y + 2][x + 2] && cell === arrayTabela[y + 3][x + 3]) {
                    const divVictory = document.createElement('div')
                    divVictory.id = 'victory'
                    divVictory.innerHTML = `${jogadorAtual} você ganhou!`
                    divMainContainer.appendChild(divVictory)
                    let x = document.createElement('div');
                    x.className = 'meteor';
                    document.getElementById('alerta').appendChild(x);
                    return true
                }
            }
        }
    }

    // DIAGONAL (DOWN LEFT)
    // iterate each row   
    for (let y = 3; y < arrayTabela.length; y++) {

        // iterate each cell in the row
        for (let x = 0; x < edgeX; x++) {
            cell = arrayTabela[y][x];

            // Only check if cell is filled
            if (cell !== 0) {
                console.log(x, y);
                console.log(arrayTabela);
                // Check the next two cells for the same value
                if (cell === arrayTabela[y - 1][x + 1] && cell === arrayTabela[y - 2][x + 2] && cell === arrayTabela[y - 3][x + 3]) {
                    const divVictory = document.createElement('div')
                    divVictory.id = 'victory'
                    divVictory.innerHTML = `${jogadorAtual} você ganhou!`
                    divMainContainer.appendChild(divVictory)
                    //insere fogos de artifício
                     let x = document.createElement('div');
                     x.className = 'meteor';
                     document.getElementById('alerta').appendChild(x);
                    return true
                }
            }
        }
    }

    return false

}
/*
function resetGame()
    Reseta a tabela para um novo jogo
    Reseta a mensagem de vitória e desaparece com o botao de RESET
    Reseta a variavel jogadorAtual para vazio
*/
function resetGame() {
    criarBotaoReset();

    const botaoReset = document.querySelector('button');
    botaoReset.addEventListener('click', reset);
}

function criarBotaoReset() {
    const elementoPai = document.getElementById("victory");
    const botaoReset = document.createElement("button");
    botaoReset.innerHTML = "Reset Game";
    botaoReset.id = 'button'
    elementoPai.appendChild(botaoReset);
}

function reset() {
    resetarBoard();
    console.log("Resetou board");

    // Romove discos
    const elementoColuna = document.querySelectorAll(".coluna");
    for (let i = 0; i < elementoColuna.length; i++) {
        while (elementoColuna[i].firstChild) {
            elementoColuna[i].removeChild(elementoColuna[i].firstChild);
        }
    }

    // Sorteia um novo jogador
    gameStart();

    // Chama novamente os eventos de click;
    addColunaEvtList();

    // Remove div victory
    const elementoMainContainer = document.getElementById("mainContainer");
    const elementoVictory = document.getElementById("victory");
    elementoMainContainer.removeChild(elementoVictory);
}

function resetarBoard() {
    for (let i = 0; i < board.length; i++) {
        for (let j = 0; j < board[0].length; j++) {
            board[i][j] = 0;
            arrayTabela[i][j] = 0;
        }
    }
}

/*
ALEX
function criaFicha()
    Reatribui as variaveis divFichaPreta e divFichaVermelha para criar as divs das Fichas
    Atribui ID e Class as fichas criadas
    Atribui style para divFichaPreta e divFichaVermelha terem backgrounds das cores respectivas
*/
function criaFicha() {
    if (jogadorAtual === "vermelha") {
        divFichaVermelha = document.createElement("div");
        divFichaVermelha.style.backgroundColor = "red";
        divFichaVermelha.className = "ficha";

        return divFichaVermelha;
    } else {
        divFichaPreta = document.createElement("div");
        divFichaPreta.style.backgroundColor = "black";
        divFichaPreta.style.backgroundImage = "url('meteoro.jpg')";
        divFichaPreta.className = "ficha";
        divFichaPreta.classList.add("preta");

        return divFichaPreta;
    }
}

/*
ALEX
function gameStart()
    Determina, em um cara ou coroa, que é o jogador que começará jogando
    Modifica a variavel jogadorAtual para determinar quem começa
    Chama a funçao criaTabela() e criaFicha() para carregar as funcionalidades primarias para o inicio do game
*/
function gameStart() {
    const sortearJogador = Math.floor(Math.random() * 2);

    if (sortearJogador === 0) {
        jogadorAtual = "vermelha";
    } else {
        jogadorAtual = "preta";
    }

    //criaTabela();
    criaFicha();
}

gameStart();

/*
ALEX
function addFicha(evt)
    Recebe o evento de clique para determinar em qual coluna a ficha será adicionada
    Adiciona a ficha no proprio arrayTabela, o mantendo atualizado com as jogadas
    Revisa a coluna para que as condiçoes de adição sejam verificadas, caso tudo certo adiciona a divFicha
    correspondente
    Verifica se a condiçao de vitoria (winCondition()) foi alcançada
*/
function addFicha(evt) {

    var colunaAtual = evt.currentTarget; // Diz onde aconteceu o click
    let colunaPai = document.getElementById(colunaAtual.id);
    let fichaAtual;

    if (colunaPai.childElementCount < 6) {
        if (jogadorAtual === "vermelha") {
            fichaAtual = criaFicha();
        } else {
            fichaAtual = criaFicha();
        }

        for (let i = 0; i < arrayColunas.length; i++) {
            if (arrayColunas[i].id === colunaAtual.id) {
                arrayColunas[i].appendChild(fichaAtual);

                let indiceColuna = colunaPai.childElementCount - 1; // Pega a quantidade de filhos da coluna
                if (fichaAtual.classList.contains("preta")) {
                    board[indiceColuna][i] = 2;
                } else {
                    board[indiceColuna][i] = 1;
                }

                if (winCondition(board)) {
                    removeEvtList();
                    resetGame();
                }

                // Define próximo jogador
                if (jogadorAtual === "vermelha") {
                    jogadorAtual = "preta";
                } else {
                    jogadorAtual = "vermelha";
                }
                break;
            }
        }
    }
}

/*
CHRYSTIAN
function criaColunas()
    Cria as divColunas, define ID e Class e as adiciona no arrayColunas
    Nao é necessario dar o appendChild() ainda
*/
function criaColunas() {
    for (let i = 0; i < arrayColunas.length; i++) {
        divColuna = document.createElement("div");
        divColuna.className = "coluna";
        divColuna.id = "coluna_" + i.toString();
        arrayColunas[i] = divColuna;
        divJogo.appendChild(divColuna);
    }
}
criaColunas();

function criaTabela() {

    // linha
    for (let x = 0; x < 7; x++) {
        // pegar colunar
        for (let y = 0; y < 6; y++) {
            // criar div
            let divElemento = document.createElement('div')
                // criar classe do elemento
            divElemento.className = 'elemento';
            divElemento.id = `elemento_${x.toString()}${y.toString()}`;
            // divElemento.style.backgroundColor = "transparent";    
            //  adicionar para cada coluna
            document.getElementById('coluna_' + x).appendChild(divElemento)

        }
    }
}
// 
// criaTabela()
/*
CHRYSTIAN
function addColunaEvtList()
    Adiciona os eventListeners de click nas colunas
*/
function addColunaEvtList() {
    for (let i = 0; i < arrayColunas.length; i++) {
        arrayColunas[i].addEventListener('click', addFicha, false);
        arrayColunas[i].addEventListener('click', floatFicha, false);
        arrayColunas[i].addEventListener('mouseover', floatFicha, false);
    }
}
addColunaEvtList();

/* Romover evento de click nas colunas e evento mouseover na divFloat */
function removeEvtList() {
    for (let i = 0; i < arrayColunas.length; i++) {
        arrayColunas[i].removeEventListener('click', addFicha);
        arrayColunas[i].removeEventListener('mouseover', floatFicha);
    }
}

function teste(evt) {
    limpaFloat(); //Limpa a area de float para adicionar a peça a coluna
    let colunaAtual = evt.currentTarget; // Diz onde aconteceu o click
    console.log(evt);
    console.log(colunaAtual.id); // Printa a id da coluna clicada
}

function floatFicha(evt) {
    //Recorta somente a parte numerica da id da coluna expecifica para usar de index
    let indice = evt.currentTarget.id.slice(7);
    let colunaAtual = evt.currentTarget.id;
    //Testes
    // console.log(evt);
    // console.log(colunaAtual.id);
    // console.log(indice);

    //Muda a cor do float correspondente ao mesmo tempo que refatora as outras cores para a base
    for (let i = 0; i < arrayColunas.length; i++) {
        if (i.toString() === indice) {
            document.getElementById("float_" + indice).style.backgroundColor = criaFicha().style.backgroundColor;
        } else {
            document.getElementById("float_" + i.toString()).style.backgroundColor = "lightblue";
        }
    }
}

/*
CHRYSTIAN
function limpaFloat()
    Repinta a div que está vermelha ou preta para a cor padrao lightblue
*/
function limpaFloat() {

    for (let i = 0; i < arrayColunas.length; i++) {
        if (document.getElementById("float_" + i.toString()).style.backgroundColor !== "lighblue") {
            document.getElementById("float_" + i.toString()).style.backgroundColor = "lightblue";
        }
    }
}